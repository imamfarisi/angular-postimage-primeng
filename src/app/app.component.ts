import { Component, HostListener } from '@angular/core';
import { ImageOption } from './component/post-image.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  previewImage = false
  activeImagePreview = 0

  data = [
    'https://wallpapercave.com/wp/wp6202096.png',
    'https://cdn.discordapp.com/attachments/1066703832136044645/1085833257788112916/-51607641763mdyza9cffc_-_Copy.png',
    'https://cdn.discordapp.com/attachments/1066703832136044645/1085833258085928970/download_-_Copy.jpeg',
    'https://cdn.discordapp.com/attachments/1066703832136044645/1085833258316607518/thumb-1920-1098682.png',
    'https://staticc.sportskeeda.com/editor/2023/03/28e35-16781392976745-1920.jpg',
    'https://images4.alphacoders.com/129/1298907.jpg',
  ]

  clickImage(index: number) {
    this.previewImage = true
    this.activeImagePreview = index
  }

  onCloseImagePreview() {
    this.previewImage = false
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(evt: KeyboardEvent) {
    this.previewImage = false
  }

  imageOptions: ImageOption[] = [
    {
      len: 1,
      imageItem: [
        { class: 'w-full h-30rem' }
      ]
    },
    {
      len: 2,
      imageItem: [
        { class: 'w-6' }, { class: 'w-6' }
      ]
    },
    {
      len: 3,
      imageItem: [
        { class: 'w-full h-30rem' }, { class: 'w-6' }, { class: 'w-6' }
      ]
    },
    {
      len: 4,
      imageItem: [
        { class: 'w-full h-15rem' }, { class: 'w-4 h-15rem' }, { class: 'w-4 h-15rem' }, { class: 'w-4 h-15rem' }
      ]
    },
  ]
}